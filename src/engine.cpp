// Plastic Engine
// Copyright (C) 2008  Albert Brown
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

// engine_core: operates subsystems when registered.  Controls timing

#include <stdexcept>
#include <iostream>
#include <string>
#include "SDL.h"

#include "state.hpp"
#include "engine.hpp"
#include "state.hpp"


void engine_subsystem::tick(state &s)
{
    // To be overloaded by derived subsystems
}


engine_core *engine_core::_instance = NULL;

engine_core::engine_core(int argc, char *argv[])
{
    // This is cleaner then waiting for the libraries to throw a fit
    if(_instance)
	throw std::logic_error("Only one engine_core instance "
			       "at a time, please.");

    // Init SDL (nothing window/video/audio related
    if(SDL_Init(SDL_INIT_EVERYTHING))
    {
	// Something went wrong
	std::string msg = std::string("SDL failed to initialize: ")
	    + SDL_GetError();
	throw std::runtime_error(msg.c_str());
    }

    std::cout << "engine_core initialized." << std::endl;
    _instance = this;
}

engine_core::~engine_core()
{
    // bring down SDL
    SDL_Quit();

    _instance = NULL;
    std::cout << "engine_core shut down." << std::endl;
}

static bool subsystem_order_sort(std::pair<int, engine_subsystem *> a,
				 std::pair<int, engine_subsystem *> b)
{
    return (a.first < b.first);
}

void engine_core::register_subsystem(engine_subsystem *sys, int order)
{
    _subsystems.push_back(std::pair<int, engine_subsystem *>(order, sys));
    _subsystems.sort(subsystem_order_sort);
}

void engine_core::release_subsystem(engine_subsystem *sys)
{
    std::list<std::pair<int, engine_subsystem *> >::iterator it;
    
    for(it = _subsystems.begin(); it != _subsystems.end(); ++it)
    {
	if(sys == (*it).second)
	{
	    _subsystems.erase(it);
	    break;
	}
    }

    if(it == _subsystems.end())
	throw std::runtime_error("Can't release subsystem not registered "
				 "with engine");
}

 
void engine_core::run()
{
    unsigned int time2;

    // TODO: when we have a conf system, we can un-hardcode these values
    _state.running = true;
    _state.time = SDL_GetTicks();
    _state.tickrate = 15;
    _state.speed = 100;

    while(_state.running)
    {
	//std::cout << _subsystems.size() << std::endl;
	// we'll never get a signal to stop if there are no active subsystems
	if(_subsystems.empty())
	{
	    std::cerr << "Run engine with no subsystems??" << std::endl;
	    break;
	}

	// Run each subsystem's 'tick' function
	std::list<std::pair<int, engine_subsystem *> >::iterator it;
	for(it = _subsystems.begin(); it != _subsystems.end(); ++it)
	    (*it).second->tick(_state);
	
	time2 = SDL_GetTicks();
	if(time2 < (_state.time += (1000 / _state.tickrate)))
	    SDL_Delay(_state.time - time2);
    }
}
