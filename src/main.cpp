// Plastic Engine
// Copyright (C) 2008  Albert Brown
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include "SDL.h"

#ifdef HAVE_CONFIG_H
    #include "config.hpp"
#else
    #define PACKAGE_STRING "Plastic"
#endif

#include "engine.hpp"
#include "window.hpp"


#define GNUMESSAGE \
    " This program is free software, covered by the GNU General Public\n"\
    " License.  It comes with no warranty. You are welcome to modify\n"\
    " and distribute the source code under certain conditions. See the\n"\
    " enclosed COPYING file for more details.\n"

int main(int argc, char *argv[])
{
    std::cout << PACKAGE_STRING << "\n\n" GNUMESSAGE "\n" << std::endl;

    // Initialize the engine core
    engine_core ec(argc, argv);
    
    {
	// Initialize subsystems
	window_subsystem wss; 

	// Register subsystems with engine
	ec.register_subsystem(&wss, 1);

	// Start the game...
	ec.run();

	// Release subsystems
	ec.release_subsystem(&wss);

    } // (subsystem instances have gone out of scope)


    return 0; // (engine_core has gone out of scope)
}
