// Plastic Engine
// Copyright (C) 2008  Albert Brown
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef WINDOW_HPP
#define WINDOW_HPP

#include "SDL.h"

#include "engine.hpp"


class window_subsystem : public engine_subsystem
{
private:
    SDL_Surface *_screen;

public:
    window_subsystem();
    ~window_subsystem();

    void tick(state &s);
};

#endif
