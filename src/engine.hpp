// Plastic Engine
// Copyright (C) 2008  Albert Brown
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <list>

#include "state.hpp"


class engine_subsystem
{
private:

public:
    virtual void tick(state &s);
};

class engine_core
{
private:
    std::list<std::pair<int, engine_subsystem *> > _subsystems;
    
    state _state;

    static engine_core *_instance;
public:
    engine_core(int argc, char *argv[]);
    ~engine_core();

    void register_subsystem(engine_subsystem *sys, int order);
    void release_subsystem(engine_subsystem *sys);

    void run();
};

#endif
