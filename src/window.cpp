// Plastic Engine
// Copyright (C) 2008  Albert Brown
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Window subsystem: establish a buffer to draw in, handle input

#include <stdexcept>
#include <iostream>
#include <string>
#include "SDL.h"

#ifdef HAVE_CONFIG_H
    #include "config.hpp"
#else
    #define PACKAGE_STRING "Plastic"
#endif

#include "state.hpp"
#include "window.hpp"



window_subsystem::window_subsystem()
{
    // TODO: we're going to have to implement commandline arguments and
    // some sort of config interface before we can un-hardcode these values
    _screen = SDL_SetVideoMode(640, 480, 32, SDL_DOUBLEBUF | SDL_OPENGL);

    if(!_screen)
    {
	// Something went wrong
	std::string msg = std::string("SDL failed to set video mode: ")
	    + SDL_GetError();
	throw std::runtime_error(msg.c_str());
    }

    SDL_WM_SetCaption(PACKAGE_STRING, PACKAGE_STRING);

    std::cout << "window_subsystem initialized." << std::endl;
}

window_subsystem::~window_subsystem()
{
    // The way SDL is designed, we can't really close the window.
    // TODO: maybe it would be possible if we init the SDL video subsystem
    //   here (in this class).  This might allow us to close and restart
    //   it aswell, although this should not be neccesary (maybe on windows).

    std::cout << "window_subsystem shut down." << std::endl;
}

void window_subsystem::tick(state &s)
{
    SDL_Event event;
    while(SDL_PollEvent(&event))
    {
	switch(event.type)
	{
	case SDL_KEYDOWN:
	    if(event.key.keysym.sym == SDLK_ESCAPE)
	    {
		std::cout << "Escape" << std::endl;
		s.running = false;
	    }
	    break;
	case SDL_QUIT:
	    std::cout << "Close button" << std::endl;
	    s.running = false;
	    break;
	}
    }
}
