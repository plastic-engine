// Plastic Engine
// Copyright (C) 2008  Albert Brown
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef STATE_HPP
#define STATE_HPP

class state 
{
public:
    state();
    ~state();
    
    // serialize/read/write file functions

    // -------- STATE INFO UNDER HERE --------
    bool running;
    unsigned int time;
    unsigned int tickrate;
    unsigned int speed;  // this may belong to the physics engine
};

#endif
